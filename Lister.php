<?php

/*
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

require_once "inc/Header.php";
require_once "inc/Notice.php";
require_once "inc/Table.php";
require_once "inc/Footer.php";

class Lister {

    function __construct() {
        
    }

    /**
     * Initialize the plugin
     */
    function initialize_plugin() {
        add_role(
          'stock_manager', 'Gestionnaire de Stock', array(
          'read' => true,
          'edit_posts' => true,
          'edit_product' => true,
          'edit_products' => true,
          'edit_published_products' => true,
          'edit_others_products' => true,
          'edit_pages' => false,
          'edit_published_posts' => false,
          'edit_published_pages' => false,
          'edit_private_pages' => false,
          'edit_private_posts' => false,
          'edit_others_posts' => false,
          'edit_others_pages' => false,
          'publish_posts' => false,
          'publish_pages' => false,
          'delete_posts' => false,
          'delete_pages' => false,
          'delete_private_pages' => false,
          'delete_private_posts' => false,
          'delete_published_pages' => false,
          'delete_published_posts' => false,
          'delete_others_posts' => false,
          'delete_others_pages' => false,
          'manage_categories' => false,
          'manage_links' => false,
          'moderate_comments' => false,
          'upload_files' => true,
          'export' => false,
          'import' => false,
          'list_users' => false,
          'edit_theme_options' => false,
          )
        );
        $this->plugin_activated();
    }

    /**
     * The function call back to
     * Register the non admin style and javascript
     * 
     */
    function register_non_admin_style() {
        wp_register_style("bootstrap4", "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css");
        wp_register_script("productlisterJS", plugins_url('/js/productlister.js', __FILE__), ["jquery"], false, false);
        wp_register_style('productlister.css', plugins_url('/css/productlister.css', __FILE__), array(), 0.1);
        wp_register_script("bootstrapJS", "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/js/bootstrap.min.js", false, true);
        wp_enqueue_style('productlister.css');

        wp_enqueue_script("bootstrapJS");
        wp_enqueue_script("productlisterJS");
        wp_localize_script('productlisterJS', 'ajaxurl', admin_url('admin-ajax.php'));
        wp_enqueue_style("bootstrap4");
    }

    /*
     * The function call back to
     * Register the admin style.
     */

    function register_admin_styles() {
        wp_register_style("bootstrap4", "https://stackpath.bootstrapcdn.com/bootstrap/4.4.1/css/bootstrap.min.css");
        wp_enqueue_style("bootstrap4");
    }

    /**
     * Activate the plugin
     */
    private function plugin_activated() {
        $this->register_style();
        $this->register_ajax();
        add_action('admin_menu', array($this, 'produt_list_menu'));
        add_shortcode('lister_product', array($this, 'shortcode_handler'));
    }

    /**
     * Register ajax action
     */
    private function register_ajax() {
        add_action('wp_ajax_nopriv_ajax_search', array($this, 'ajax_search'));
        add_action('wp_ajax_nopriv_ajax_pagination', array($this, 'ajax_pagination'));
        add_action('wp_ajax_ajax_update', array($this, 'ajax_update'));
        add_action('wp_ajax_ajax_create', array($this, 'ajax_create'));
        add_action('wp_ajax_nopriv_ajax_category', array($this, 'ajax_category'));
        add_action('wp_ajax_ajax_category', array($this, 'ajax_category'));
        add_action('wp_ajax_ajax_remove', array($this, 'ajax_remove'));
        add_action('wp_ajax_ajax_pagination', array($this, 'ajax_pagination'));
        add_action('wp_ajax_ajax_search', array($this, 'ajax_search'));
    }

    /**
     * Add the register style action
     */
    private function register_style() {
        add_action("wp_enqueue_scripts", array($this, "register_non_admin_style"));
        add_action('admin_enqueue_scripts', array($this, 'register_admin_styles'));
    }

    /**
     * The deactivation call back function that revove the role
     */
    function plugin_deactivated() {
        remove_role('stock_manager');
    }

    /**
     * Get the header 'search  filter, reset and create new product'
     * @return type html, The header of the table.
     */
    private function getHeader() {
        $header = new Header;
        return $header->get_html();
    }

    /**
     * Get the product table with action or not
     * @param type $number The pagination number of the asked page
     * @param type $search The searching product if the search is active
     * @param type $category The category if filtered
     * @return type html, the html code of the table to show
     */
    private function getTable($number = 0, $search = "", $category = 0) {

        $table = new Table;
        return $table->get_html($number, $search, $category);
    }

    /**
     * Get the pagination of the product table
     * @param type $number The pagination number of the asked page
     * @param type $search The search query if there is one
     * @param type $category The category if there is one
     * @return type html, the pagination with the correct page
     */
    private function getFooter($number, $search, $category) {
        $footer = new Footer;
        return $footer->get_html($number, $search, $category);
    }

    /**
     * Get the full html to show for the product table
     * @param type $number The pagination number of the asked page
     * @param type $postPerPage Post per page we want to show
     * @param type $search The search query 
     * @param type $category The category if filtered
     * @param type $header The header if we want to generate the header
     * @return string The generated html to show.
     */
    private function getHTML($number = 0, $postPerPage, $search = "", $category = 0, $header = true) {
        $html = '<div class = "row">';
        if ($header) {
            $html = $html . $this->getHeader();
        }
        $html = $html . $this->getTable($number, $search, $category);
        $html = $html . $this->getFooter($number, $search, $category);
        $html = $html . '</div>';
        return $html;
    }

    /**
     * Handle the shortcode
     */
    function shortcode_handler() {
        if (!is_admin()) {
            echo $this->getHTML(0, 5, "", 0, true);
            die();
        }
    }

    /**
     * Show the admin notice if the woocommerce plugin is not activated
     * @global type $pagenow The page we are in.
     */
    function general_admin_notice() {

        if (is_admin()) {
            $notice = new Notice;
            echo $notice->get_html();
        }
    }

    /**
     * The ajax function callback that remove the product
     * return type int  1 if success 0 if not
     */
    function ajax_remove() {
        $data = stripslashes($_POST['param']);
        $param = json_decode($data);

        if (is_user_logged_in() && current_user_can('edit_posts') && current_user_can('manage_options') && current_user_can('delete_posts')) {
            if (wp_delete_post($param->id, true)) {
                echo 1;
            }
            else {
                echo 0;
            }
        }
        else {
            echo 0;
        }
        die();
    }

    /**
     * The ajax pagination function call back
     */
    function ajax_pagination() {
        $data = stripslashes($_POST['param']);
        $param = json_decode($data);
        $category = $this->getCategory($param);
        $search = $this->getSearch($param);

        if (is_numeric($param->number)) {
            echo $this->getHTML(esc_html($param->number), 5, $search, $category, false);
        }
        die();
    }

    /**
     * Extract the category from ajax parameter
     * @param type $param The ajax parameter
     * @return if success the category, 0  if not
     */
    function getCategory($param) {

        if (isset($param->category)) {
            return esc_html($param->category);
        }
        return 0;
    }

    /**
     * Extract the search parameter in ajax parameter
     * @param type $param The ajax parameter
     * @return string The search query if there is
     */
    function getSearch($param) {
        if (isset($param->search)) {
            return esc_html($param->search);
        }
        return "";
    }

    /**
     * The ajax function callback that handle the 
     */
    function ajax_category() {
        $data = stripslashes($_POST['param']);
        $param = json_decode($data);

        $category = $this->getCategory($param);

        $search = $this->getSearch($param);

        echo $this->getHTML(0, -1, $search, $category, false);


        die();
    }

    function ajax_update() {

        $stock = stripslashes($_POST['stock']);
        $name = stripslashes($_POST["name"]);
        $price = stripslashes($_POST["price"]);
        $colors = stripslashes($_POST["colorTag"]);
        $cat = stripslashes($_POST["catTag"]);
        $filename = $_FILES['file']['name'];
        if ($filename != "") {
            $imageFileType = pathinfo($location, PATHINFO_EXTENSION);
            $upload_overrides = array("test_form" => false);
            $upload_file = wp_handle_upload($_FILES["file"], $upload_overrides);

            if ($upload_file['error']) {
                echo $upload_file['error'];
                die();
            }

            $wp_filetype = wp_check_filetype($filename, null);
            $attachment = array(
              'post_mime_type' => $wp_filetype['type'],
              'post_parent' => 0,
              'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
              'post_content' => '',
              'post_status' => 'inherit'
            );
            $attachment_id = wp_insert_attachment($attachment, $upload_file['file'], null, true);

            if (!is_wp_error($attachment_id)) {

                require_once(ABSPATH . "wp-admin" . '/includes/image.php');
                $attachment_data = wp_generate_attachment_metadata($attachment_id, $upload_file['file']);
                wp_update_attachment_metadata($attachment_id, $attachment_data);


                $success = set_post_thumbnail($_POST["id"], $attachment_id);
            }



            update_post_meta($_POST["id"], '_product_image', $attachment_id);
        }
        $post = array(
          'ID' => $_POST["id"],
          'post_title' => $name,
        );
        wp_update_post($post);
        update_post_meta($post_id, '_manage_stock', true);
        wc_update_product_stock(wc_get_product($_POST["id"]), $stock, 'set', true);
        update_post_meta($_POST["id"], '_price', (float) $price);
        $colors = explode(',', trim($colors, " \n\t\r\0\x0B,"));
        $index = 0;
        foreach ($colors as $color) {
            if (term_exists($color, "pa_colors", null) != null) {
                $term = term_exists($color, "pa_colors", null);
                if ($index === 0) {
                    $append = false;
                }
                else {
                    $append = true;
                }
                wp_set_post_terms($_POST["id"], $color, 'pa_colors', $append);
                $index++;
            }
        }


        $cat = explode(',', trim($cat, " \n\t\r\0\x0B,"));
        $index = 0;
        foreach ($cat as $category) {
            if (term_exists($category, "product_cat", null) != null) {
                $term = term_exists($category, "product_cat", null);
                if ($index === 0) {
                    $append = false;
                }
                else {
                    $append = true;
                }
                wp_set_post_terms($_POST["id"], $term, 'product_cat', $append);
                $index++;
            }
        }

        echo true;
        die();
    }

    function ajax_create() {
        $stock = stripslashes($_POST['stock']);
        $name = stripslashes($_POST["name"]);
        $price = stripslashes($_POST["price"]);
        $colors = stripslashes($_POST["colorTag"]);
        $cat = stripslashes($_POST["catTag"]);
        $filename = $_FILES['file']['name'];
        $args = array(
          'post_author' => get_current_user_id(),
          'post_content' => '',
          'post_status' => "Publish", // (Draft | Pending | Publish)
          'post_title' => $name,
          'post_parent' => '',
          'post_type' => "product"
        );


        $post_id = wp_insert_post($args);


        wp_set_object_terms($post_id, 'simple', 'product_type');
        if ($filename != "") {
            $imageFileType = pathinfo($location, PATHINFO_EXTENSION);
            $upload_overrides = array("test_form" => false);
            $upload_file = wp_handle_upload($_FILES["file"], $upload_overrides);

            if ($upload_file['error']) {
                echo $upload_file['error'];
                die();
            }

            $wp_filetype = wp_check_filetype($filename, null);
            $attachment = array(
              'post_mime_type' => $wp_filetype['type'],
              'post_parent' => 0,
              'post_title' => preg_replace('/\.[^.]+$/', '', $filename),
              'post_content' => '',
              'post_status' => 'inherit'
            );
            $attachment_id = wp_insert_attachment($attachment, $upload_file['file'], null, true);

            if (!is_wp_error($attachment_id)) {
                require_once(ABSPATH . "wp-admin" . '/includes/image.php');
                $attachment_data = wp_generate_attachment_metadata($attachment_id, $upload_file['file']);
                wp_update_attachment_metadata($attachment_id, $attachment_data);
                $success = set_post_thumbnail($_POST["id"], $attachment_id);
            }
            set_post_thumbnail($post_id, $attachment_id);
        }
        update_post_meta($post_id, '_manage_stock', true);
        $att_color = Array('pa_colors' => Array(
            'name' => 'pa_colors',
            'value' => "",
            'is_visible' => '1',
            'is_taxonomy' => '1'
        ));

        update_post_meta($post_id, '_product_attributes', $att_color);
        wc_update_product_stock(wc_get_product($post_id), $stock, 'set', true);
        update_post_meta($post_id, '_price', (float) $price);
        $colors = explode(',', trim($colors, " \n\t\r\0\x0B,"));
        $index = 0;
        foreach ($colors as $color) {
            if (term_exists($color, "pa_colors", null) != null) {
                $term = term_exists($color, "pa_colors", null);
                if ($index === 0) {
                    $append = false;
                }
                else {
                    $append = true;
                }
                wp_set_post_terms($post_id, $color, 'pa_colors', $append);
                $index++;
            }
        }


        $cat = explode(',', trim($cat, " \n\t\r\0\x0B,"));
        $index = 0;
        foreach ($cat as $category) {
            if (term_exists($category, "product_cat", null) != null) {
                $term = term_exists($category, "product_cat", null);
                if ($index === 0) {
                    $append = false;
                }
                else {
                    $append = true;
                }
                wp_set_post_terms($post_id, $term, 'product_cat', $append);
                $index++;
            }
        }

        echo true;
        die();
    }

    /**
     * Ajax search function call back 
     */
    function ajax_search() {
        $data = stripslashes($_POST['param']);
        $param = json_decode($data);
        $category = $this->getCategory($param);
        $search = $this->getSearch($param);
        echo $this->getHTML(0, -1, $search, $category, false);

        die();
    }

    /**
     * 
     * @return string
     */
    function productList_admin_page_function() {
        return "";
    }

    /**
     * 
     */
    function produt_list_menu() {
        add_menu_page('Product List Options', 'Product List', 'manage_options', 'ProductList-Plugin', array($this, 'product_list_options'), "data:image/svg+xml;base64,PHN2ZyBpZD0iQ2FscXVlXzEiIGRhdGEtbmFtZT0iQ2FscXVlIDEiIHhtbG5zPSJodHRwOi8vd3d3LnczLm9yZy8yMDAwL3N2ZyIgdmlld0JveD0iMCAwIDQxNy4wMzg3NiAyMjguMjUxOTkiPjx0aXRsZT5QbHVnaW4gbG9nbzwvdGl0bGU+PHJlY3QgeD0iMi41IiB5PSIxNTcuOTc5NDIiIHdpZHRoPSIyNTAuODA2ODQiIGhlaWdodD0iNjcuNzcyNTciIHN0eWxlPSJmaWxsOiNmZmY7c3Ryb2tlOiMwMDA7c3Ryb2tlLW1pdGVybGltaXQ6MTA7c3Ryb2tlLXdpZHRoOjVweCIvPjxwYXRoIGQ9Ik02NTIuMjcxLDQxMS4xMjI5MXY3Ljg2NUg1OTIuNjA2Mjd2LTcuODY1QTExLjc3NjQzLDExLjc3NjQzLDAsMCwxLDYwNC4zNDkyMiwzOTkuMzhoMzYuMTc4ODNBMTEuNzcxMzksMTEuNzcxMzksMCwwLDEsNjUyLjI3MSw0MTEuMTIyOTFaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNTc5LjI4OTk4IC0yNjEuMDA4NDkpIiBzdHlsZT0iZmlsbDojZmZmO3N0cm9rZTojMDAwO3N0cm9rZS1taXRlcmxpbWl0OjEwO3N0cm9rZS13aWR0aDo1cHgiLz48cGF0aCBkPSJNNzM3LjAyNTc5LDQxMS4xMjI5MXY3Ljg2NUg2NzcuMzYxMDZ2LTcuODY1QTExLjc3MTM4LDExLjc3MTM4LDAsMCwxLDY4OS4xMDQsMzk5LjM4aDM2LjE3ODg3QTExLjc3NjQ2LDExLjc3NjQ2LDAsMCwxLDczNy4wMjU3OSw0MTEuMTIyOTFaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNTc5LjI4OTk4IC0yNjEuMDA4NDkpIiBzdHlsZT0iZmlsbDojZmZmO3N0cm9rZTojMDAwO3N0cm9rZS1taXRlcmxpbWl0OjEwO3N0cm9rZS13aWR0aDo1cHgiLz48cGF0aCBkPSJNODIxLjc4MDUzLDQxMS4xMjI5MXY3Ljg2NUg3NjIuMTA4di03Ljg2NUExMS43NzgxLDExLjc3ODEsMCwwLDEsNzczLjg1ODc1LDM5OS4zOGgzNi4xNzEwOEExMS43NzgwNiwxMS43NzgwNiwwLDAsMSw4MjEuNzgwNTMsNDExLjEyMjkxWiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTU3OS4yODk5OCAtMjYxLjAwODQ5KSIgc3R5bGU9ImZpbGw6I2ZmZjtzdHJva2U6IzAwMDtzdHJva2UtbWl0ZXJsaW1pdDoxMDtzdHJva2Utd2lkdGg6NXB4Ii8+PHJlY3QgeD0iODEuOTgxMDUiIHk9IjkwLjIwNjg2IiB3aWR0aD0iMjUwLjgwNjg0IiBoZWlnaHQ9IjY3Ljc3MjU3IiBzdHlsZT0iZmlsbDojZmZmO3N0cm9rZTojMDAwO3N0cm9rZS1taXRlcmxpbWl0OjEwO3N0cm9rZS13aWR0aDo1cHgiLz48cGF0aCBkPSJNNzMxLjc1MjA2LDM0My4zNTAzNXY3Ljg2NUg2NzIuMDg3MzJ2LTcuODY1YTExLjc3NjQzLDExLjc3NjQzLDAsMCwxLDExLjc0My0xMS43NDI5MUg3MjAuMDA5MUExMS43NzE0LDExLjc3MTQsMCwwLDEsNzMxLjc1MjA2LDM0My4zNTAzNVoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC01NzkuMjg5OTggLTI2MS4wMDg0OSkiIHN0eWxlPSJmaWxsOiNmZmY7c3Ryb2tlOiMwMDA7c3Ryb2tlLW1pdGVybGltaXQ6MTA7c3Ryb2tlLXdpZHRoOjVweCIvPjxwYXRoIGQ9Ik04MTYuNTA2ODUsMzQzLjM1MDM1djcuODY1SDc1Ni44NDIxMXYtNy44NjVBMTEuNzcxMzksMTEuNzcxMzksMCwwLDEsNzY4LjU4NSwzMzEuNjA3NDRoMzYuMTc4ODdBMTEuNzc2NDYsMTEuNzc2NDYsMCwwLDEsODE2LjUwNjg1LDM0My4zNTAzNVoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC01NzkuMjg5OTggLTI2MS4wMDg0OSkiIHN0eWxlPSJmaWxsOiNmZmY7c3Ryb2tlOiMwMDA7c3Ryb2tlLW1pdGVybGltaXQ6MTA7c3Ryb2tlLXdpZHRoOjVweCIvPjxwYXRoIGQ9Ik05MDEuMjYxNTksMzQzLjM1MDM1djcuODY1SDg0MS41ODkwNXYtNy44NjVhMTEuNzc4MTEsMTEuNzc4MTEsMCwwLDEsMTEuNzUwNzYtMTEuNzQyOTFoMzYuMTcxMDdBMTEuNzc4MDcsMTEuNzc4MDcsMCwwLDEsOTAxLjI2MTU5LDM0My4zNTAzNVoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC01NzkuMjg5OTggLTI2MS4wMDg0OSkiIHN0eWxlPSJmaWxsOiNmZmY7c3Ryb2tlOiMwMDA7c3Ryb2tlLW1pdGVybGltaXQ6MTA7c3Ryb2tlLXdpZHRoOjVweCIvPjxyZWN0IHg9IjE2My43MzE5MiIgeT0iMjIuMTA3OTEiIHdpZHRoPSIyNTAuODA2ODQiIGhlaWdodD0iNjcuNzcyNTciIHN0eWxlPSJmaWxsOiNmZmY7c3Ryb2tlOiMwMDA7c3Ryb2tlLW1pdGVybGltaXQ6MTA7c3Ryb2tlLXdpZHRoOjVweCIvPjxwYXRoIGQ9Ik04MTMuNTAyOTIsMjc1LjI1MTR2Ny44NjVINzUzLjgzODE5di03Ljg2NWExMS43NzY0MiwxMS43NzY0MiwwLDAsMSwxMS43NDI5NS0xMS43NDI5MUg4MDEuNzZBMTEuNzcxMzksMTEuNzcxMzksMCwwLDEsODEzLjUwMjkyLDI3NS4yNTE0WiIgdHJhbnNmb3JtPSJ0cmFuc2xhdGUoLTU3OS4yODk5OCAtMjYxLjAwODQ5KSIgc3R5bGU9ImZpbGw6I2ZmZjtzdHJva2U6IzAwMDtzdHJva2UtbWl0ZXJsaW1pdDoxMDtzdHJva2Utd2lkdGg6NXB4Ii8+PHBhdGggZD0iTTg5OC4yNTc3MSwyNzUuMjUxNHY3Ljg2NUg4MzguNTkzdi03Ljg2NWExMS43NzEzOCwxMS43NzEzOCwwLDAsMSwxMS43NDI5MS0xMS43NDI5MWgzNi4xNzg4N0ExMS43NzY0NSwxMS43NzY0NSwwLDAsMSw4OTguMjU3NzEsMjc1LjI1MTRaIiB0cmFuc2Zvcm09InRyYW5zbGF0ZSgtNTc5LjI4OTk4IC0yNjEuMDA4NDkpIiBzdHlsZT0iZmlsbDojZmZmO3N0cm9rZTojMDAwO3N0cm9rZS1taXRlcmxpbWl0OjEwO3N0cm9rZS13aWR0aDo1cHgiLz48cGF0aCBkPSJNOTgzLjAxMjQ1LDI3NS4yNTE0djcuODY1SDkyMy4zMzk5MnYtNy44NjVhMTEuNzc4MSwxMS43NzgxLDAsMCwxLDExLjc1MDc1LTExLjc0MjkxaDM2LjE3MTA4QTExLjc3ODA1LDExLjc3ODA1LDAsMCwxLDk4My4wMTI0NSwyNzUuMjUxNFoiIHRyYW5zZm9ybT0idHJhbnNsYXRlKC01NzkuMjg5OTggLTI2MS4wMDg0OSkiIHN0eWxlPSJmaWxsOiNmZmY7c3Ryb2tlOiMwMDA7c3Ryb2tlLW1pdGVybGltaXQ6MTA7c3Ryb2tlLXdpZHRoOjVweCIvPjxjaXJjbGUgY3g9IjE4MC44NTYzNSIgY3k9IjQwLjUyNDM5IiByPSI3LjkyOTY0IiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMDAwO3N0cm9rZS1taXRlcmxpbWl0OjEwO3N0cm9rZS13aWR0aDo2cHgiLz48bGluZSB4MT0iMTgwLjg1NjM1IiB5MT0iNTguNDk4MjMiIHgyPSIxODAuODU2MzUiIHkyPSIxMjguMjc5MDQiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiMwMDA7c3Ryb2tlLW1pdGVybGltaXQ6MTA7c3Ryb2tlLXdpZHRoOjZweCIvPjxjaXJjbGUgY3g9IjI2MS43Mzg2NiIgY3k9Ijg1Ljk4NzY0IiByPSI0OS42OTI0IiBzdHlsZT0iZmlsbDpub25lO3N0cm9rZTojMDAwO3N0cm9rZS1taXRlcmxpbWl0OjEwO3N0cm9rZS13aWR0aDo2cHgiLz48Y2lyY2xlIGN4PSIxNDkuNDAyMTIiIGN5PSIxMjYuOTU3NDQiIHI9IjkuMjUxMjQiIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiMwMDA7c3Ryb2tlLW1pdGVybGltaXQ6MTA7c3Ryb2tlLXdpZHRoOjZweCIvPjxsaW5lIHgxPSIxMjUuODc3NTMiIHkxPSIxMTcuNzA2MTkiIHgyPSIxMjUuODc3NTMiIHkyPSIyMTguMTQ4MjciIHN0eWxlPSJmaWxsOm5vbmU7c3Ryb2tlOiMwMDA7c3Ryb2tlLW1pdGVybGltaXQ6MTA7c3Ryb2tlLXdpZHRoOjZweCIvPjwvc3ZnPg==");
    }

    /**
     * Show the tutoriel
     */
    function product_list_options() {
        if (!current_user_can('manage_options')) {
            wp_die(__('You do not have sufficient permissions to access this page.'));
        }
        $html = '<div class="row p-0 m-0">'
          . '
            <div class="col-3">
            <svg id="Calque_1" class="mt-2 w-50" data-name="Calque 1" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 417.03876 228.25199"><title>Plugin logo</title><rect x="2.5" y="157.97942" width="250.80684" height="67.77257" style="fill:#fff;stroke:#000;stroke-miterlimit:10;stroke-width:5px"/><path d="M652.271,411.12291v7.865H592.60627v-7.865A11.77643,11.77643,0,0,1,604.34922,399.38h36.17883A11.77139,11.77139,0,0,1,652.271,411.12291Z" transform="translate(-579.28998 -261.00849)" style="fill:#fff;stroke:#000;stroke-miterlimit:10;stroke-width:5px"/><path d="M737.02579,411.12291v7.865H677.36106v-7.865A11.77138,11.77138,0,0,1,689.104,399.38h36.17887A11.77646,11.77646,0,0,1,737.02579,411.12291Z" transform="translate(-579.28998 -261.00849)" style="fill:#fff;stroke:#000;stroke-miterlimit:10;stroke-width:5px"/><path d="M821.78053,411.12291v7.865H762.108v-7.865A11.7781,11.7781,0,0,1,773.85875,399.38h36.17108A11.77806,11.77806,0,0,1,821.78053,411.12291Z" transform="translate(-579.28998 -261.00849)" style="fill:#fff;stroke:#000;stroke-miterlimit:10;stroke-width:5px"/><rect x="81.98105" y="90.20686" width="250.80684" height="67.77257" style="fill:#fff;stroke:#000;stroke-miterlimit:10;stroke-width:5px"/><path d="M731.75206,343.35035v7.865H672.08732v-7.865a11.77643,11.77643,0,0,1,11.743-11.74291H720.0091A11.7714,11.7714,0,0,1,731.75206,343.35035Z" transform="translate(-579.28998 -261.00849)" style="fill:#fff;stroke:#000;stroke-miterlimit:10;stroke-width:5px"/><path d="M816.50685,343.35035v7.865H756.84211v-7.865A11.77139,11.77139,0,0,1,768.585,331.60744h36.17887A11.77646,11.77646,0,0,1,816.50685,343.35035Z" transform="translate(-579.28998 -261.00849)" style="fill:#fff;stroke:#000;stroke-miterlimit:10;stroke-width:5px"/><path d="M901.26159,343.35035v7.865H841.58905v-7.865a11.77811,11.77811,0,0,1,11.75076-11.74291h36.17107A11.77807,11.77807,0,0,1,901.26159,343.35035Z" transform="translate(-579.28998 -261.00849)" style="fill:#fff;stroke:#000;stroke-miterlimit:10;stroke-width:5px"/><rect x="163.73192" y="22.10791" width="250.80684" height="67.77257" style="fill:#fff;stroke:#000;stroke-miterlimit:10;stroke-width:5px"/><path d="M813.50292,275.2514v7.865H753.83819v-7.865a11.77642,11.77642,0,0,1,11.74295-11.74291H801.76A11.77139,11.77139,0,0,1,813.50292,275.2514Z" transform="translate(-579.28998 -261.00849)" style="fill:#fff;stroke:#000;stroke-miterlimit:10;stroke-width:5px"/><path d="M898.25771,275.2514v7.865H838.593v-7.865a11.77138,11.77138,0,0,1,11.74291-11.74291h36.17887A11.77645,11.77645,0,0,1,898.25771,275.2514Z" transform="translate(-579.28998 -261.00849)" style="fill:#fff;stroke:#000;stroke-miterlimit:10;stroke-width:5px"/><path d="M983.01245,275.2514v7.865H923.33992v-7.865a11.7781,11.7781,0,0,1,11.75075-11.74291h36.17108A11.77805,11.77805,0,0,1,983.01245,275.2514Z" transform="translate(-579.28998 -261.00849)" style="fill:#fff;stroke:#000;stroke-miterlimit:10;stroke-width:5px"/><circle cx="180.85635" cy="40.52439" r="7.92964" style="fill:none;stroke:#000;stroke-miterlimit:10;stroke-width:6px"/><line x1="180.85635" y1="58.49823" x2="180.85635" y2="128.27904" style="fill:none;stroke:#000;stroke-miterlimit:10;stroke-width:6px"/><circle cx="261.73866" cy="85.98764" r="49.6924" style="fill:none;stroke:#000;stroke-miterlimit:10;stroke-width:6px"/><circle cx="149.40212" cy="126.95744" r="9.25124" style="fill:none;stroke:#000;stroke-miterlimit:10;stroke-width:6px"/><line x1="125.87753" y1="117.70619" x2="125.87753" y2="218.14827" style="fill:none;stroke:#000;stroke-miterlimit:10;stroke-width:6px"/></svg>
            </div><div class="col-12">';
        $html = $html . '<h1 class="text-center w-100">How to use this plugin ?</h1>';
        $html = $html . "<div class='col-12'><h3 class='w-100 text-center'>Step 1</h3>"
          . "<p class='w-100 text-center'>1. Create a new page and put the title you want.</p>"
          . "<p class='w-100 text-center'>2. In the content of the page, put the following shortcode : <strong >[lister_product]</strong> </p></div>"
          . "<div class='col-12'><h3 class='w-100 text-center'>Step 2</h3>"
          . "<p class='w-100 text-center'>1. Go to the user tab .</p>"
          . "<p class='w-100 text-center'>2. Choose the user you want to give the role : <strong>Gestionnaire de stock</strong></p>"
          . "<p class='w-100 text-center'>3. Change the role he has.</p></div>"
          . "<div class='col-12'><h3 class='w-100 text-center'>Step 3</h3>"
          . "<p class='w-100 text-center'>1. You are ready to go. He can only update or create new product.</p>"
          . "<p class='w-100 text-center'>2. Go to the page link where you put the shortcode and <strong>enjoy</strong></p></div>";
        $html = $html . '</div></div>';
        echo $html;
    }

    /**
     * Add the admin notices action
     */
    function show_admin_notice() {
        add_action('admin_notices', array($this, 'general_admin_notice'));
    }

}
