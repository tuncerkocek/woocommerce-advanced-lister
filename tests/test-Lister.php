<?php

/**
 * Class SampleTest
 *
 * @package Woocommerce_Product_List
 */

/**
 * Sample test case.
 */
class Lister extends WP_UnitTestCase {

    public function test_has_role_stock_manager() {
        $lister = new App\Lister;
        $lister->initialize_plugin();
        $this->assertTrue($GLOBALS['wp_roles']->is_role("stock_manager"));
    }

    public function test_has_register_style() {
        $lister = new App\Lister;
        $lister->initialize_plugin();
        $this->assertTrue(has_action("wp_enqueue_scripts"));
    }

    public function test_has_admin_register_style() {
        $lister = new App\Lister;
        $lister->initialize_plugin();
        $this->assertTrue(has_action("admin_enqueue_scripts"));
    }

    public function test_has_no_priv_ajax_category() {
        $lister = new App\Lister;
        $lister->initialize_plugin();
        $this->assertTrue(has_action("wp_ajax_nopriv_ajax_category"));
    }

    public function test_has_no_priv_ajax_search() {
        $lister = new App\Lister;
        $lister->initialize_plugin();
        $this->assertTrue(has_action("wp_ajax_nopriv_ajax_search"));
    }

    public function test_has_no_priv_ajax_pagination() {
        $lister = new App\Lister;
        $lister->initialize_plugin();
        $this->assertTrue(has_action("wp_ajax_nopriv_ajax_pagination"));
    }

    public function test_has_priv_ajax_category() {
        $lister = new App\Lister;
        $lister->initialize_plugin();
        $this->assertTrue(has_action("wp_ajax_ajax_category"));
    }

    public function test_has_priv_ajax_search() {
        $lister = new App\Lister;
        $lister->initialize_plugin();
        $this->assertTrue(has_action("wp_ajax_ajax_search"));
    }

    public function test_has_priv_ajax_pagination() {
        $lister = new App\Lister;
        $lister->initialize_plugin();
        $this->assertTrue(has_action("wp_ajax_ajax_pagination"));
    }

    public function test_has_priv_ajax_remove() {
        $lister = new App\Lister;
        $lister->initialize_plugin();
        $this->assertTrue(has_action("wp_ajax_ajax_remove"));
    }

    public function test_has_admin_notice() {
      
        $this->assertTrue(has_action("admin_notices"));
    }

    public function test_has_admin_menu() {
       
        $this->assertTrue(has_action("admin_menu"));
    }

        public function test_has_shortcode() {
    
        $this->assertTrue(has_shortcode("[lister_product]","lister_product"));
    }
    /*     public function test_has_not_role_stock_manager() {
      $lister = new App\Lister;
      $lister->initialize_plugin();
      //activate_plugins('/woocommerce-product-list/woocommerce-product-list.php');
      //deactivate_plugins('/woocommerce-product-list/woocommerce-product-list.php');
      //  $this->assertFalse($GLOBALS['wp_roles']->is_role("stock_manager"));
      } */
}
