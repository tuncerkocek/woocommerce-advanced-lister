<?php

/*
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

class Footer {

    function __construct() {
        
    }

    function get_html($number, $search, $category) {

        $args = array('post_type' => 'product', 'posts_per_page' => -1, "product_cat" => $category, 's' => $search);

        $post = query_posts($args);

        //  if (count($post) - $postPerPage > 0) {
        $html = '<div class="col-6">
<nav aria-label="Page navigation product">
     
        <ul class="pagination">';
        $loop = $number;
        if ($number > 0) {
            $html = $html . '<li class="page-item"><a class="page-link btn" id="' . ($number - 1) . '">Previous</a></li>';
        }
        $stayPost = (count($post) - (($number + 1) * 5)) % 5;


        $next = false;
        while ($loop < $stayPost) {
            if ($number == 0) {
                $next = true;
            }
            $html = $html . '<li class="page-item"><a class="page-link btn" id="' . $loop . '">' . ($loop + 1) . '</a></li>';

            $loop++;
        }

        if ($next) {
            $html = $html . '<li class = "page-item"><a class = "page-link btn" id = "' . ($number + 1) . '">Next</a></li>';
        }
        $html = $html . '</ul>
        </nav>
        </div></div>';

        return $html;
    }

}
