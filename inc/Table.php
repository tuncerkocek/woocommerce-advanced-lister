<?php

/*
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

class Table {

    function __construct() {
        
    }

    function get_html($number, $search, $category) {
        $args = array('post_type' => 'product', ''
          . 'posts_per_page' => 5,
          "offset" => 5 * $number,
          "product_cat" => $category,
          's' => $search);
        $post = query_posts($args);
        $html = '<div class = "col-12 table-row"><table class = "table">
<thead>
<tr>
<th scope = "col " class="text-center">Image</th>
<th scope = "col" class="text-center">Name</th>
<th scope = "col" class="text-center">Stock</th>
<th scope = "col" class="text-center">Price</th>
<th scope = "col" class="text-center">Color</th>
<th scope = "col" class="text-center">Category</th>';
        if (is_user_logged_in() && user_can(get_current_user_id(), 'edit_posts')) {
            $html = $html . '<th scope = "col">Action</th>';
        }
        $html = $html . '</tr>
</thead>
<tbody>';
        $i = 0;

        while ($i < count((Array) $post)) {

            $product = wc_get_product($post[$i]->ID);
            $attachments_id = $product->get_image_id();
            $image_url = wp_get_attachment_url($attachments_id);



            $term = $this->get_colors($product);
            $html = $html . $this->get_row($product, $post, $image_url, $i, $term);
            $argsCat = array(
              'taxonomy' => "product_cat",
              'hide_empty' => false
            );
            $all_categories = get_terms($argsCat);

            $html = $html . $this->get_editable_row($post, $product, $all_categories, $i);

            $i++;
        }
        $html = $html . '</tbody>
</table>';

        return $html;
    }

    private function get_editable_row($post, $product, $all_categories, $i) {
        $html = '<div class = " modal post-edit-' . $post[$i]->ID . '" tabindex="-1" role="dialog" aria-labelledby="ModalTitle" aria-hidden="true">
                <div class="modal-dialog " role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title">Edit</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                <div class="modal-body p-3">
                      <div class="row w-100 m-0 p-0">
                        <div class="col-3">
                            <h5>Image :</h5>
                        </div>
                        <div class="col-9" >
                            <input type="file" class="m-0 p-0 file-' . $post[$i]->ID . '" /></div>
                        </div>
                        <div class="row w-100 m-0 p-0  mt-2">
                            <div class="col-3"><h5>Nom :</h5></div>
                                <div class="col-9" > 
                                    <input type="text" class="name-' . $post[$i]->ID . '" value="' . $product->get_name() . '"/>
                                </div>
                            </div>
                            <div class="row w-100 m-0 p-0 mt-2">
                                <div class="col-3">
                                    <h5>Stock :</h5>
                                </div>
                                <div class="col-9" >  
                                    <input type="number" class="stock-' . $post[$i]->ID . '" value="' . number_format($product->get_stock_quantity(), 0, '', '') . '"/>
                                </div>
                            </div>
                            <div class="row w-100 m-0 p-0 mt-2">
                                <div class="col-3">
                                    <h5>Prix :</h5>
                                </div>
                                <div class="col-9" >     
                                    <input type="number" class="price-' . $post[$i]->ID . '" value="' . $product->get_price() . '"/>
                                </div>
                            </div>
                            <div class="row m-0 p-0 w-100 mt-2">
                                <div class="col-3">
                                    <h5>Color :</h5>
                                </div>
                                <div class="col-6" >     
                                    <select class="custom-select w-100 form-control custom-select-lg color-select-' . $post[$i]->ID . '">';
        $argsColor = array(
          'taxonomy' => "pa_colors",
          'hide_empty' => false
        );
        $product_colors = get_terms($argsColor);

        foreach ($product_colors as $color) {
            $html = $html . '<option value="' . $color->name . '">' . $color->name . '</option>';
        }
        $product_colors = [];
        $termNumber = 0;
        while (array_key_exists("pa_colors", $product->get_attributes()) && $product->get_attributes()["pa_colors"]["options"] != null && $termNumber < count($product->get_attributes()["pa_colors"]["options"])) {
            $color = get_term($product->get_attributes()["pa_colors"]["options"][$termNumber], "pa_colors")->name;
            array_push($product_colors, $color);
            $termNumber++;
        }
        $html = $html . '</select/>'
          . '       </div>'
          . '                   <div class="col-3" >
                                    <button class="btn btn-success mt-2 add-color-tag" id="' . $post[$i]->ID . '">Add</button>
                                </div>' .
          $this->tag("color-content-" . $post[$i]->ID, $product_colors, "color", $post[$i]->ID) . '</div>
                                <div class="row m-0 p-0 w-100 mt-2">
                                   <div class="col-3">
                                      <h5>Category :</h5>
                                   </div>
                                   <div class="col-6" >
                                    <select class="custom-select w-100 category-select-' . $post[$i]->ID . ' form-control custom-select-lg">';

        foreach ($all_categories as $category) {

            $html = $html . '<option value="' . $category->name . '">' . $category->name . '</option>';
        }

        $html = $html . '</select/>'
          . '           </div>'
          . '<div class="col-3" >'
          . '               <button class="btn btn-success mt-2 add-cat-tag" id="' . $post[$i]->ID . '">Add</button>'
          . '           </div>';
        $html = $html . $this->tag("category-content-" . $post[$i]->ID, explode(",", strip_tags(wc_get_product_category_list($post[$i]->ID))), "category", $post[$i]->ID);

        $html = $html . '<div class="modal-footer">'
          . '               <button class="btn btn-success save" id="' . $post[$i]->ID . '">Save</button></div>';

        $html = $html . '     </div>
                            </div>
                           </div>
                           </div>';
        return $html;
    }

    private function tag($tagname, $categories = [], $sectionName, $id) {

        $html = '<div class=" ' . $tagname . ' col-12 ">';


        foreach ($categories as $category) {
            if ($category != "") {
                $html = $html . '<div class="row mt-2 "><div class="col-3"><p class=" ' . $sectionName . '-tag-' . $id . '">' . $category . '</p></div> '
                  . '<div class="col-2">'
                  . '   <button class="btn btn-light delete-tag" >Remove</button>'
                  . '</div>'
                  . '</div>';
            }
        }
        $html = $html . '</div>';
        return $html;
    }

    private function get_colors($product) {
        $termNumber = 0;
        $term = "";

        while (array_key_exists("pa_colors", $product->get_attributes()) && $product->get_attributes()["pa_colors"]["options"] != null && $termNumber < count($product->get_attributes()["pa_colors"]["options"])) {
            $color = get_term($product->get_attributes()["pa_colors"]["options"][$termNumber], "pa_colors")->name;
            if ($termNumber == 0) {
                $term = $color . "<div class='rectangle mx-auto' style='background-color:" . $color . "'></div><br>";
            }
            else {
                $term = $term . $color . "<div class='rectangle mx-auto' style='background-color:" . $color . "'></div><br>";
            }

            $termNumber++;
        }


        return $term;
    }

    private function get_row($product, $post, $image_url, $i, $term) {
        $html = '<tr class = "post-' . $post[$i]->ID . '">
                 
                               <td class = "text-center"><img class = "preview-product"  src = "' . $image_url . '"/></td>
                               <td class = "text-center">' . $product->get_name() . '</td>
                               <td class = "text-center">' . number_format($product->get_stock_quantity(), 0, '', '') . '</td>
                                <td class = "text-center">' . wc_price($product->get_price()) . '</td>
                                <td class = "text-center">' . $term . '</td>
                                <td class = "text-center">' . wc_get_product_category_list($post[$i]->ID) . '</td>';
        if (is_user_logged_in() && user_can(get_current_user_id(), 'edit_posts')) {
            $html = $html . ' <td><div class = "row">
                                <div class = "col-12 text-center mt-2">
                                    <button class = "edit btn btn-success  text-white" id="' . $post[$i]->ID . '">Edit</button>
                                </div>
                            <div class = "col-12 text-center mt-2">';
            if (current_user_can('manage_options')) {
                $html = $html . '<button class = "remove btn btn-danger" id="' . $post[$i]->ID . '">remove</button>';
            }
            $html = $html .
              '</div>
                                </div>
                                </td>';
        }
        $html = $html . '</tr>';
        return $html;
    }

}
