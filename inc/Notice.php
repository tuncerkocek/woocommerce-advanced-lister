<?php

/*
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

class Notice {

    function __construct() {
        
    }

    function get_html() {

        return '<div class = "notice notice-warning is-dismissible">
        <p>Woocommerce is not active. Please activate it otherwise Woocommerce Product Lister will not work properly</p>
        </div>';
    }

}
