<?php

/*
  This program is free software: you can redistribute it and/or modify
  it under the terms of the GNU General Public License as published by
  the Free Software Foundation, either version 3 of the License, or
  (at your option) any later version.

  This program is distributed in the hope that it will be useful,
  but WITHOUT ANY WARRANTY; without even the implied warranty of
  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
  GNU General Public License for more details.

  You should have received a copy of the GNU General Public License
  along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

namespace App;

class Header {

    function __construct() {
        
    }

    function get_html() {
        $argsCat = array(
          'taxonomy' => "product_cat",
          'hide_empty' => false
        );
        $product_categories = get_terms($argsCat);
        $html = '
    <div class="col-5">
                    <nav class="navbar p-0 m-0">
                        <div class="form-inline w-100">
                            <input class="form-control w-50 search-input" type="search" placeholder="Search" aria-label="Search">
                            <button class="search btn ml-2 w-25 btn-outline-success my-2 my-sm-0" type="submit">Search</button>
                        </div>
                    </nav>
                </div>
                     <div class="col-5">
                        <nav class="navbar p-0 m-0">
                        <div class="form-inline w-100">
                        <select class="custom-select w-50 form-control custom-select-lg">';
        $html = $html . '<option value="0" selected>All categories</option>';
        foreach ($product_categories as $category) {

            $html = $html . '<option value="' . $category->name . '">' . $category->name . '</option>';
        }
        $html = $html . '</select>
                        <button class="filter ml-2 w-25 btn btn-outline-success ">Filter</button>
                    </div>
                </nav>
                </div>
                 <div class="col-2 text-left mt-lg-2">
                    <button class="btn btn-danger reset d-none text-white"> Reset all</a>
                </div>';
        if (is_user_logged_in() && current_user_can('edit_posts')) {
            $html = $html . '  <div class="col-12 text-right mb-3">
                    <button class="btn btn-success text-white create-product" > Create a new product</a>
                </div>' . $this->get_editable_row();
        }

        return $html;
    }

    private function get_editable_row() {
        $html = '<div class = " modal add-modal" tabindex="-1" role="dialog" aria-labelledby="ModalTitle" aria-hidden="true">
                <div class="modal-dialog " role="document">
                    <div class="modal-content">
                      <div class="modal-header">
                        <h5 class="modal-title">Edit</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                <div class="modal-body p-3">
                      <div class="row w-100 m-0 p-0">
                        <div class="col-3">
                            <h5>Image :</h5>
                        </div>
                        <div class="col-9" >
                            <input type="file" class="m-0 p-0 add-file" /></div>
                        </div>
                        <div class="row w-100 m-0 p-0  mt-2">
                            <div class="col-3"><h5>Nom* :</h5></div>
                                <div class="col-9" > 
                                    <input type="text" class="add-name" value=""/>
                                </div>
                            </div>
                            <div class="row w-100 m-0 p-0 mt-2">
                                <div class="col-3">
                                    <h5>Stock :</h5>
                                </div>
                                <div class="col-9" >  
                                    <input type="number" class="add-stock" value="0"/>
                                </div>
                            </div>
                            <div class="row w-100 m-0 p-0 mt-2">
                                <div class="col-3">
                                    <h5>Prix :</h5>
                                </div>
                                <div class="col-9" >     
                                    <input type="number" class="add-price" value="0"/>
                                </div>
                            </div>
                            <div class="row m-0 p-0 w-100 mt-2">
                                <div class="col-3">
                                    <h5>Color :</h5>
                                </div>
                                <div class="col-6" >     
                                    <select class="custom-select w-100 form-control custom-select-lg add-color-select">';
        $argsColor = array(
          'taxonomy' => "pa_colors",
          'hide_empty' => false
        );
        $product_colors = get_terms($argsColor);
        foreach ($product_colors as $color) {
            $html = $html . '<option value="' . $color->name . '">' . $color->name . '</option>';
        }
        $html = $html . '</select/>'
          . '       </div>'
          . '                   <div class="col-3" >
                                    <button class="btn btn-success mt-2 add-color-add-tag">Add</button>
                                </div>' .
          $this->tag("add-color-content") . '</div>
                                <div class="row m-0 p-0 w-100 mt-2">
                                   <div class="col-3">
                                      <h5>Category :</h5>
                                   </div>
                                   <div class="col-6" >
                                    <select class="custom-select w-100 add-category-select form-control custom-select-lg">';
        $argsCat = array(
          'taxonomy' => "product_cat",
          'hide_empty' => false
        );
        $all_categories = get_terms($argsCat);
        foreach ($all_categories as $category) {

            $html = $html . '<option value="' . $category->name . '">' . $category->name . '</option>';
        }

        $html = $html . '</select/>'
          . '           </div>'
          . '<div class="col-3" >'
          . '               <button class="btn btn-success mt-2 add-cat-add-tag" id="">Add</button>'
          . '           </div>' . $this->tag("add-category-content");
        $html = $html . '<div class="modal-footer">'
          . '               <button class="btn btn-success create">Create</button></div>';

        $html = $html . '     </div>
                            </div>
                           </div>
                           </div></div>';
        return $html;
    }

    private function tag($tagname) {
        return '<div class=" ' . $tagname . ' col-12 "></div>';
    }

}
