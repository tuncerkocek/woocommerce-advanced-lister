<?php

/**
 * Plugin Name: Woocommerce Product Lister
 * Plugin URI: 
 * Description: This plugin permit to the user to have full access to the product list and to manage them easily;
 * Version: 1.0
 * Author: Pixelio
 * Author URI: www.pixelio.be
 */
require_once "Lister.php";
if (!function_exists('add_action')) {
    echo 'Hi there!  Security breach not found';
    exit;
}

$lister = new App\Lister;
if (in_array('woocommerce/woocommerce.php', apply_filters('active_plugins', get_option('active_plugins')))) {

    $lister->initialize_plugin();
}
else {
    $lister->show_admin_notice();
}

register_deactivation_hook(__FILE__, 'plugin_deactivated');

function plugin_deactivated() {
    remove_role('stock_manager');
}
