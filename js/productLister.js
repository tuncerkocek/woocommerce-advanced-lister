
/*
 This program is free software: you can redistribute it and/or modify
 it under the terms of the GNU General Public License as published by
 the Free Software Foundation, either version 3 of the License, or
 (at your option) any later version.
 
 This program is distributed in the hope that it will be useful,
 but WITHOUT ANY WARRANTY; without even the implied warranty of
 MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 GNU General Public License for more details.
 
 You should have received a copy of the GNU General Public License
 along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

(function ($) {
    $(document).ready(function () {

        let searchGlobal = "";
        let categoryGlobal = "";
        function deleteTag() {
            $(".delete-tag").unbind();
            $(".delete-tag").click(function () {
                $(this).parent().parent().remove();
            });
        }
        function attach() {

            $(".edit").click(function () {
                let id = $(this).attr("id");

                $(".post-edit-" + id).modal('show');
            });

            deleteTag();
            $(".add-color-tag").click(function () {
                let id = $(this).attr("id");
                let color = $(".color-select-" + id).val();
                var already = false;
                $(".color-tag-" + id).each(function () {
                    if ($(this).text() == color) {
                        already = true;
                    }
                });
                if (already) {
                    alert("The color is already added");
                } else
                if (color != "") {
                    $(".color-content-" + id).append("<div class='row mt-2'><div class='col-3 '><p class='color-tag-" + id + "'>" + color + "</p></div><div class='col-2'><button class='btn btn-light delete-tag'>Remove</button></div></div>");
                }

                deleteTag();
            });

            $(".save").click(function () {
                let id = $(this).attr("id");
                let catTag = [];
                let colorTag = [];

                $(".color-tag-" + id).each(function () {
                    colorTag.push($(this).text());
                });
                $(".category-tag-" + id).each(function () {
                    catTag.push($(this).text());
                });
                var formData = new FormData();
                let name = $(".name-" + id).val();
                let file = $(".file-" + id)[0].files[0];
                let stock = $(".stock-" + id).val();
                let price = $(".price-" + id).val();
                formData.append("file", file);
                formData.append("name", name);
                formData.append("stock", stock);
                formData.append("price", price);
                formData.append("catTag", catTag);
                formData.append("colorTag", colorTag);
                formData.append("id", id);
                formData.append('action', 'ajax_update');
                $.ajax({
                    url: ajaxurl,
                    type: 'POST',
                    contentType: false,
                    processData: false,
                    data: formData
                }).done(function (response) {
                    if (response == 1) {
                        $(".modal").modal('hide');
                        search(0);
                    } else if (response != "") {
                        alert(response);
                    }


                });
            });
            $(".add-cat-tag").click(function () {
                let id = $(this).attr("id");
                let category = $(".category-select-" + id).val();
                let already = false;
                $(".category-tag-" + id).each(function () {
                    if ($(this).text() == category) {
                        already = true;
                    }
                });
                if (already) {
                    alert("The category is already added");
                } else if (category != "") {
                    $(".category-content-" + id).append("<div class='row mt-2'><div class='col-3 '><p class='category-tag-" + id + "'>" + category + "</p></div><div class='col-2'><button class='btn btn-light delete-tag'>Remove</button></div></div>");
                }
                deleteTag();
            });

            $(".remove").click(function () {
                let id = $(this).attr("id");
                jQuery.post(
                        ajaxurl,
                        {
                            'contentType': "application/json; charset=utf-8",
                            'action': 'ajax_remove',
                            'param': JSON.stringify({id: id})
                        },
                        function (response) {
                            if (response == 1) {
                                $(".post-" + id).remove();
                            } else {
                                alert("Une erreur s'est produite lors de la suppression. Veuillez-vous être sur d'être connecté et d'avoir les privilèges.")
                            }
                        }
                );
            });
            $(".page-link").click(function () {
                let number = $(this).attr("id");
                search(number);
            });
        }
        ;
        $(".search").click(function () {
            searchGlobal = $(".search-input").val();
            jQuery.post(
                    ajaxurl,
                    {
                        'contentType': "application/json; charset=utf-8",
                        'action': 'ajax_search',
                        'param': JSON.stringify({category: categoryGlobal, search: searchGlobal})
                    },
                    function (response) {
                        if (response != "") {
                            $(".table-row").empty();
                            $(".table-row").append(response);
                            $(".reset").removeClass("d-none");
                            attach();
                        }
                    },
                    );
        });
        $(".filter").click(function () {
            categoryGlobal = $(".custom-select").val();
            jQuery.post(
                    ajaxurl,
                    {
                        'contentType': "application/json; charset=utf-8",
                        'action': 'ajax_category',
                        'param': JSON.stringify({category: categoryGlobal, search: searchGlobal})
                    },
                    function (response) {
                        if (response != "") {
                            $(".table-row").empty();
                            console.log(response);
                            $(".table-row").append(response);
                            $(".reset").removeClass("d-none");
                            attach();
                        }
                    },
                    );
        });
        $(".reset").click(function () {
            categoryGlobal = 0;
            $(".search-input").val("");
            searchGlobal = "";
            search(0);
            attach();
            $(this).addClass("d-none");
        });
        function search(number) {
            jQuery.post(
                    ajaxurl,
                    {
                        'contentType': "application/json; charset=utf-8",
                        'action': 'ajax_pagination',
                        'param': JSON.stringify({number: number, category: categoryGlobal, search: searchGlobal})
                    },
                    function (response) {
                        if (response != "") {
                            $(".table-row").empty();
                            $(".table-row").append(response);
                            attach();
                        }
                    },
                    );
        }
        $(".create-product").click(function () {
            $(".add-modal").modal();
        });
        $(".create").click(function () {
            let catTag = [];
            let colorTag = [];
            $(".add-color-add-tag-txt").each(function () {
                colorTag.push($(this).text());
            });
            $(".add-category-add-tag-txt").each(function () {
                catTag.push($(this).text());
            });
            var formData = new FormData();
            let name = $(".add-name").val();
            if (name == "" || name == undefined) {
                alert("Please fill the mandatory name field");
                return;
            }
            let file = $(".add-file")[0].files[0];
            let stock = $(".add-stock").val();
            let price = $(".add-price").val();
            formData.append("file", file);
            formData.append("name", name);
            formData.append("stock", stock);
            formData.append("price", price);
            formData.append("catTag", catTag);
            formData.append("colorTag", colorTag);

            formData.append('action', 'ajax_create');
            $.ajax({
                url: ajaxurl,
                type: 'POST',
                contentType: false,
                processData: false,
                data: formData
            }).done(function (response) {
                if (response == 1) {
                    $(".modal").modal('hide');
                    search(0);
                } else if (response != "") {
                    alert(response);
                }


            });
        });
        $(".add-color-add-tag").click(function () {

            let color = $(".add-color-select").val();
            let already = false;
            $(".add-color-add-tag-txt").each(function () {
                if ($(this).text() == color) {
                    already = true;
                }
            });
            if (already) {
                alert("The color is already added");
            } else if (color != "") {
                $(".add-color-content").append("<div class='row mt-2'><div class='col-3 '><p class='add-color-add-tag-txt'>" + color + "</p></div><div class='col-2'><button class='btn btn-light delete-tag'>Remove</button></div></div>");
            }
            deleteTag();

        });
        $(".add-cat-add-tag").click(function () {
            let id = $(this).attr("id");
            let category = $(".add-category-select").val();
            var already = false;
            $(".add-category-add-tag-txt").each(function () {
                if ($(this).text() == category) {
                    already = true;
                }
            });
            if (already) {
                alert("The category is already added");
            } else if (category != "") {
                $(".add-category-content").append("<div class='row mt-2'><div class='col-3 '><p class='add-category-add-tag-txt'>" + category + "</p></div><div class='col-2'><button class='btn btn-light delete-tag'>Remove</button></div></div>");
            }
            deleteTag();

        });
        attach();
    });
})(jQuery);

